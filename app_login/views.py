from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import User
from django.urls import reverse
# Create your views here.

response = {}

def index(request):# pragma: no cover
    if 'user_login' in request.session:
        response['login'] = True
        return HttpResponseRedirect(reverse('app-mahasiswa:profile'))

    else :
        response['login'] = False
        html = 'app_login/session/login.html'
        return render(request, html, response)


def profile(request):# pragma: no cover
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('app-login:index'))
    else:
        kode_identitas = request.session['kode_identitas']
        try:
            user = User.objects.get(kode_identitas = kode_identitas)
            set_data_for_session(user)
        except Exception as e:
            user = User()
            user.kode_identitas = kode_identitas
            user.username = request.session['user_login']
            user.save()
            set_data_for_session(user)

        html = 'app_login/session/profile.html'
        return render(request, html, response)

def set_data_for_session(user):# pragma: no cover
    response['author'] = user.username
    response['npm'] = user.kode_identitas
    response['foto_profil'] = user.foto_profil
    response['nama'] = user.nama
    response['profile_linkedin'] = user.profile_linkedin
    response['email'] = user.email
