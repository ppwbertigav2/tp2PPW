from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class Lab10UnitTest(TestCase):

    def test_url_is_exist(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, index)
