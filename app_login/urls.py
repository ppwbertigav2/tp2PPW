from .views import *
from django.conf.urls import url
from .custom_auth import auth_login, auth_logout

urlpatterns = [
	
	url(r'^$', index, name='index'),
	url(r'^custom_auth/login/$', auth_login, name='auth_login'),
	url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
	url(r'^profile/$', profile, name='profile'),
	
]
