from django.db import models

# Create your models here.

class User(models.Model):
    kode_identitas = models.CharField('npm', max_length=20, primary_key=True,)
    nama = models.CharField('nama', max_length=200, default='None')
    username = models.CharField('username',max_length=200,default='None')
    profile_linkedin = models.CharField('profile_linkedin', max_length=200, default='None')
    foto_profil = models.CharField('profpic', max_length=240, default='https://heatherchristenaschmidt.files.wordpress.com/2011/09/facebook_no_profile_pic2-jpg.gif')
    email = models.CharField('email', max_length=200, default='None')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Status(models.Model):
    new_status = models.TextField(max_length=280)
    user = models.ForeignKey(User, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
