from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .models import *
from .views import response
from .views_riwayat import *
from .views_status import *

# bikin views.py buat masing" fitur di sini ya, nanti kalo mau dipake ke urls.py jangan lupa di import dulu filenya

def profile(request):# pragma: no cover
	print ("#==> profile")

	if not 'user_login' in request.session.keys():
		return HttpResponseRedirect('/login/')
	else:
		kode_identitas = request.session['kode_identitas']
		try:
			pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
			set_data_for_session(pengguna)
			list_matkul = get_matkul()

		except Exception as e:
			pengguna = Pengguna()
			pengguna.kode_identitas = kode_identitas
			pengguna.username = request.session['user_login']
			pengguna.save()
			set_data_for_session(pengguna)
			list_matkul = get_matkul()

		number_of_status = Status.objects.filter(pengguna__kode_identitas=kode_identitas).count()
		response['total'] = number_of_status
		if response['total'] != 0:
			last_post = Status.objects.latest("created_date")
			response['last'] = last_post

		list_keahlian = get_my_keahlian_from_database(request)
		response['list_matkul'] = list_matkul
		response['list_keahlian'] = list_keahlian

		status(request)

		html = 'session/mahasiswaPage.html'
		return render(request, html, response)

def set_data_for_session(user):# pragma: no cover
    response['username'] = user.username
    response['npm'] = user.kode_identitas
    response['foto_profil'] = user.foto_profil
    response['nama'] = user.nama
    response['profile_linkedin'] = user.profile_linkedin
    response['email'] = user.email

def get_my_keahlian_from_database(request):# pragma: no cover
    resp = []
    kode_identitas = request.session['kode_identitas']
    pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    items = Keahlian.objects.filter(pengguna=pengguna)
    for item in items:
        resp.append(item)
    return resp

def edit_profile_page(request):# pragma: no cover
    if not 'user_login' in request.session:
        return HttpResponseRedirect(reverse('app-mahasiswa:index'))
    else:
        list_keahlian = get_my_keahlian_from_database(request)
        response['list_keahlian'] = list_keahlian
        html = 'session/edit_profile.html'
        return render(request, html, response)

def add_keahlian_to_database(request, id_skill, id_level):# pragma: no cover
    skills = ['Java', 'C#', 'Python', 'Erlang', 'Kotlin']
    levels = ['Beginner', 'Intermediate', 'Advanced', 'Expert', 'Legend']
    kode_identitas = request.session['kode_identitas']
    pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    objKeahlian = Keahlian()
    objKeahlian.keahlian = skills[int(id_skill)]
    objKeahlian.level = levels[int(id_level)]
    objKeahlian.pengguna = pengguna
    objKeahlian.save()
    return HttpResponseRedirect('/mahasiswa/edit-profile/')

def delete_keahlian(request, id):# pragma: no cover
    instance = Keahlian.objects.get(id=id)
    instance.delete()
    return HttpResponseRedirect('/mahasiswa/edit-profile/')
